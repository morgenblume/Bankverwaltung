
public class Konto {
	private int kontonummer;
	private float kontostand;
	
	public int getKontonummer() {
		return kontonummer;		
	}
	public void setKontonummer(int kontonummer){
		this.kontonummer = kontonummer;
	}
	public float getKontostand() {
		return kontostand;
	}
	public void setKontostand(float kontostand) {
		this.kontostand = kontostand;
	}

}
